/** Bài 1: Tính tiền lương nhân viên */
    // input
    var workingDay = 28;
    // output
    var wage = 0;
    /**progress:
     * wage = aDayWage * workingDay
     */
    const aDayWage = 100000;
    wage = aDayWage * workingDay;
    console.log(`Tổng tiền lương là : ${wage}`);
/**Bài 2: Tính giá trị trung bình */
    // input
    var no_1 = 1;
    var no_2 = 2;
    var no_3 = 4;
    var no_4 = 8;
    var no_5 = 16;
    // output
    var average;
    /**progress:
     * trung bình = Tổng 5 số thực / 5
     */
    average = (no_1 + no_2 + no_3 + no_4 + no_5) / 5;
    console.log(`Giá trị trung bình của 5 số thực là: ${average}`);
/**Bài 3: Quy dổi tiền */
    // input: số tiền USD
    var USDAmount = 100;
    // output: số tiền quy đổi sang VND
    var VNDAmount = 0;
    /**progress:
     * VNDAmount = VNDExchangeRate * USDAmount
     */
    var VNDExchangeRate = 23500;
    VNDAmount = USDAmount *VNDExchangeRate;
    console.log(`USD ${USDAmount} = VND ${VNDAmount}`);
/**Bài 4: Tính diện tích chu vi hình chữ nhật */
    // input: chiều dài, chiều rộng
    var length = 12;
    var width = 5;
    // output: chu vi hcn
    var perimeter = 0;
    var area = 0;
    /**progress:
     * chu vi = (dài + rộng) * 2
     * diện tích = dài * rộng
     */
    perimeter = (length + width) * 2;
    area = length*width;
    console.log(`chu vi : ${perimeter} - diện tích : ${area}`);
/**Bài 5: Tính tổng 2 ký số */
    // input: 1 số có 2 chữ số
    var no = 24;
    // output: tổng 2 ký số
    var sum = 0;
    //progress
    var dozenNo = Math.floor(no/10);
    var unitNo = no%10;
    var sum = dozenNo + unitNo;
    console.log(`Tổng 2 ký số của ${no} = ${sum}`); 


